<?php require 'header.php'; ?>

<div class="row">
  <div class="col s12 center-align title-page">
    <h1 class="grey-text text-darken-1">Contactez-nous</h1>
  </div>
</div>
<div class="section">
    <div class="contact">
      <a class="valign-wrapper grey-text text-darken-1" href="0506070809"><i class="material-icons">call</i> 05 06 07 08 09</a>
      <p class="valign-wrapper grey-text text-darken-1" id="adresse"><i class="material-icons">local_post_office</i> 5, rue du burger à BurgerVille</p>
    </div>

    <div class="horaire">
      <div class="div-icone">
        <i class="material-icons grey-text text-darken-1 horloge">access_time</i>
      </div>
      <div class="div-horaire">
        <ul id="horaire" class="grey-text text-darken-1">
          <li>Lundi : 10h00-14h00/17h00-23h30</li>
          <li>Mardi : 10h00-14h00/17h00-23h30</li>
          <li>Mercredi : 10h00-14h00/17h00-23h30</li>
          <li>Jeudi : 10h00-14h00/17h00-23h30</li>
          <li>Vendredi : 10h00-14h00/17h00-23h30</li>
          <li>Samedi : 10h00-14h00/17h00-23h30</li>
          <li>Dimanche : Fermé</li>
        </ul>
      </div>
    </div>
    <div class="map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11551.357170525307!2d2.2691927!3d43.6307037!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbca3bff3135035fc!2sCimeti%C3%A8re%20de%20La%20Barque!5e0!3m2!1sen!2sfr!4v1627465572708!5m2!1sen!2sfr" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
</div>

<?php require 'footer.php'; ?>
