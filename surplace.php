<?php require 'header.php'; ?>

<div class="row">
  <div class="col s12 center-align title-page">
    <h1 class="grey-text text-darken-1">Le click'n collect des burgers</h1>
  </div>
</div>
<div class="red lighten-5 row">
   <div class="col s12 m6 l6 xl6">
    <div class="carte red lighten-4">
      <div class="carte-img">
        <img src="https://www.atelierdeschefs.com/media/recette-e29584-burger-veggie-haricots-rouges-et-mozza.jpg">
      </div>
      <div class="carte-content">
        <div class="row">
          <div class="col s12 m12 l6 burger">
            <h2 class="grey-text text-darken-2">Burger 1</h2>
          </div>
          <div class="col s12 m12 l6 right-align div-dispo">
             <a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>
          </div>
          <div class="col s12 center-align">
            <p class="block-text grey-text text-darken-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultricies, sapien sed vulputate tincidunt, metus magna elementum nulla, id congue lorem dolor sit amet nunc.
              Maecenas lobortis enim in libero aliquam consectetur. Mauris ut tortor eu sem varius lacinia.</p>

            <p class="block-text grey-text text-darken-2">Prix: <span class="prix">10</span>€</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="red lighten-5 row">
     <div class="col s12 m6 l6 xl6">
      <div class="carte red lighten-4">
        <div class="carte-img">
          <img src="https://www.atelierdeschefs.com/media/recette-e29584-burger-veggie-haricots-rouges-et-mozza.jpg">
        </div>
        <div class="carte-content">
          <div class="row">
            <div class="col s12 m12 l6 burger">
              <h2 class="grey-text text-darken-2">Burger 2</h2>
            </div>
            <div class="col s12 m12 l6 right-align div-dispo">
               <a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>
            </div>
            <div class="col s12 center-align">
              <p class="block-text grey-text text-darken-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultricies, sapien sed vulputate tincidunt, metus magna elementum nulla, id congue lorem dolor sit amet nunc.
                Maecenas lobortis enim in libero aliquam consectetur. Mauris ut tortor eu sem varius lacinia.</p>
              <p class="block-text grey-text text-darken-2">Prix: <span class="prix">10</span>€</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="red lighten-5 row">
       <div class="col s12 m6 l6 xl6">
        <div class="carte  red lighten-4">
          <div class="carte-img">
            <img src="https://www.atelierdeschefs.com/media/recette-e29584-burger-veggie-haricots-rouges-et-mozza.jpg">
          </div>
          <div class="carte-content">
            <div class="row">
              <div class="col s12 m12 l6 burger">
                <h2 class="grey-text text-darken-2">Burger 3</h2>
              </div>
              <div class="col s12 m12 l6 right-align div-dispo">
                 <a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>
              </div>
              <div class="col s12 center-align">
                <p class="block-text grey-text text-darken-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultricies, sapien sed vulputate tincidunt, metus magna elementum nulla, id congue lorem dolor sit amet nunc.
                  Maecenas lobortis enim in libero aliquam consectetur. Mauris ut tortor eu sem varius lacinia.</p>
                <p class="block-text grey-text text-darken-2">Prix: <span class="prix">10</span>€</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="red lighten-5 row">
         <div class="col s12 m6 l6 xl6">
          <div class="carte  red lighten-4">
            <div class="carte-img">
              <img src="https://www.atelierdeschefs.com/media/recette-e29584-burger-veggie-haricots-rouges-et-mozza.jpg">
            </div>
            <div class="carte-content">
              <div class="row">
                <div class="col s12 m12 l6 burger">
                  <h2 class="grey-text text-darken-2">Burger 4</h2>
                </div>
                <div class="col s12 m12 l6 right-align div-dispo">
                   <a class="btn-floating btn-large waves-effect waves-light red"><i class="material-icons">add</i></a>
                </div>
                <div class="col s12 center-align">
                  <p class="block-text grey-text text-darken-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ultricies, sapien sed vulputate tincidunt, metus magna elementum nulla, id congue lorem dolor sit amet nunc.
                    Maecenas lobortis enim in libero aliquam consectetur. Mauris ut tortor eu sem varius lacinia.</p>
                  <p class="block-text grey-text text-darken-2">Prix: <span class="prix">10</span>€</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div class="col s12 center-align">
    <a href="order.php" class="waves-effect waves-light btn red lighten-2 btn-navigation">Retour</a>
  </div>
</div>

<?php require 'footer.php'; ?>
