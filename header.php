<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CDC</title>
    <script src="assets/jquery/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link rel="stylesheet" href="assets/style.css">
  </head>
  <body>
    <div class="navbar-fixed">
        <nav>
          <div class="nav-wrapper red lighten-2">
            <a href="#" data-target="mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="left hide-on-med-and-down main-nav red lighten-2">
              <li><a href="index.php">Accueil</a></li>
              <li><a href="menu.php">Nos burgers</a></li>
              <li><a href="order.php">Commande</a></li>
              <li><a href="history.php">Notre histoire</a></li>
              <li><a href="contact.php">Contact</a></li>
            </ul>
          </div>
        </nav>
      </div>

      <ul class="sidenav red lighten-2" id="mobile">
        <li><a href="index.php">Accueil</a></li>
        <li><a href="menu.php">Nos burgers</a></li>
        <li><a href="order.php">Commande</a></li>
        <li><a href="history.php">Notre histoire</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
