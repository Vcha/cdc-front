  <?php require 'header.php'; ?>

  <div class="main-content">
    <div class="hero-image">
      <div class="hero-text">
        <h1>Dead Cow Diner</h1>
        <a class="valign-wrapper" href="0506070809"><i class="material-icons">call</i> 05 06 07 08 09</a>
        <p class="valign-wrapper" id="adresse"><i class="material-icons">local_post_office</i> 5, rue du burger à BurgerVille</p>
      </div>
    </div>

      <div class="infos red lighten-5">
        <hr class="grey darken-1">
        <div id="ouverture">
          <h2 class="grey-text text-darken-1">Nous sommes actuellement ouvert !</h2>
        </div>
        <hr class="grey darken-1">
        <div class="row">
          <div class="col s12 l6 xl6 call-to-order">
            <h2 class="grey-text text-darken-1">Menu à partir de <span class="red-text text-lighten-2">10</span>€</h2>
            <a href="menu.php" id="burgers" class="waves-effect waves-light btn red lighten-2">Voir nos burgers</a>
            <a href="order.php" id="commande" class="waves-effect waves-light btn red lighten-2">Passer commande</a>
          </div>
          <div class="col s12 l6 xl6 affichage-horaire">
            <div class="row">
              <div class="col s12 l5 xl5 right-align">
                <i class="material-icons grey-text text-darken-1 horloge">access_time</i>
              </div>
              <div class="col s12 l7 xl7">
                <ul id="horaire" class="grey-text text-darken-1">
                  <li>Lundi : 10h00-14h00/17h00-23h30</li>
                  <li>Mardi : 10h00-14h00/17h00-23h30</li>
                  <li>Mercredi : 10h00-14h00/17h00-23h30</li>
                  <li>Jeudi : 10h00-14h00/17h00-23h30</li>
                  <li>Vendredi : 10h00-14h00/17h00-23h30</li>
                  <li>Samedi : 10h00-14h00/17h00-23h30</li>
                  <li>Dimanche : Fermé</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
  <?php require 'footer.php'; ?>
