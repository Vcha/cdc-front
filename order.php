<?php require 'header.php'; ?>

<div class="main">
  <div class="row">
    <div class="col s12 center-align title-page">
      <h1 class="grey-text text-darken-1">Pour ma commande je souhaite : </h1>
    </div>
  </div>
  <div class="btn-group">
    <div class="order">
      <a href="surplace.php" class="waves-effect waves-light btn red lighten-2">Allez sur place !</a>
    </div>
    <div class="order">
      <a href="https://www.ubereats.com/fr/store/la-fabrique-du-burger/z39-zWpLRfWzZFRd-GS2Hg" class="waves-effect waves-light btn red lighten-2">Restez chez moi !</a>
    </div>
  </div>
</div>

<?php require 'footer.php'; ?>
