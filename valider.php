<?php require 'header.php'; ?>

<div class="display">
  <p class="grey-text text-darken-1">Bravo, votre commande est enregistrée !</p>
  <a href="index.php" class="waves-effect waves-light btn red lighten-2 btn-navigation">Retour</a>
</div>

<?php require 'footer.php'; ?>
