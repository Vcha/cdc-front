<?php require 'header.php'; ?>

  <div class="row div-histoire">
    <div class="col s12 title-page center-align">
      <h1 class="grey-text text-darken-1">Notre histoire</h1>
    </div>
    <div class="col s12">
      <p class="block-text grey-text text-darken-2">
            <img src="assets/img/history.jpg" id="img-histoire" alt="main qui tient un burger délicieux">
            Nunc commodo commodo tortor non rhoncus. Quisque elementum ac urna quis tempor.
            Vestibulum sed cursus quam. Etiam porta pharetra euismod. Nunc non diam velit.
            Vivamus vehicula mi ac pretium bibendum. Integer in arcu sit amet est hendrerit auctor.
            In metus turpis, rhoncus eu elementum quis, euismod blandit sapien.
            Sed quis odio id mi dapibus vulputate. Maecenas nec ultrices mauris.
            In enim magna, tempor nec neque pulvinar, ullamcorper volutpat quam.
            Nunc commodo commodo tortor non rhoncus. Quisque elementum ac urna quis tempor.
            Vestibulum sed cursus quam. Etiam porta pharetra euismod. Nunc non diam velit.
            Vivamus vehicula mi ac pretium bibendum. Integer in arcu sit amet est hendrerit auctor.
            In metus turpis, rhoncus eu elementum quis, euismod blandit sapien.
            Sed quis odio id mi dapibus vulputate. Maecenas nec ultrices mauris.
            In enim magna, tempor nec neque pulvinar, ullamcorper volutpat quam.
            Nunc commodo commodo tortor non rhoncus. Quisque elementum ac urna quis tempor.
            Vestibulum sed cursus quam. Etiam porta pharetra euismod.
            Nunc commodo commodo tortor non rhoncus. Quisque elementum ac urna quis tempor.
            Vestibulum sed cursus quam. Etiam porta pharetra euismod. Nunc non diam velit.
            Vivamus vehicula mi ac pretium bibendum. Integer in arcu sit amet est hendrerit auctor.
            In metus turpis, rhoncus eu elementum quis, euismod blandit sapien.
            Sed quis odio id mi dapibus vulputate. Maecenas nec ultrices mauris.
            In enim magna, tempor nec neque pulvinar, ullamcorper volutpat quam.
            Nunc commodo commodo tortor non rhoncus. Quisque elementum ac urna quis tempor.
            Vestibulum sed cursus quam. Etiam porta pharetra euismod.
            Nunc commodo commodo tortor non rhoncus. Quisque elementum ac urna quis tempor.
            Vestibulum sed cursus quam. Etiam porta pharetra euismod. Nunc non diam velit.
            Vivamus vehicula mi ac pretium bibendum. Integer in arcu sit amet est hendrerit auctor.
            In metus turpis, rhoncus eu elementum quis, euismod blandit sapien.
      </p>
    </div>
  </div>
  <div class="row">
    <div class="col s12 title-page center-align">
      <h1 class="grey-text text-darken-1">Notre expertise du goût</h1>
    </div>
    <div class="col s12">
      <p class="block-text grey-text text-darken-2">
        Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
        In hac habitasse platea dictumst. Integer id sagittis orci. Suspendisse ullamcorper dictum purus
        ut porttitor. Duis elementum auctor velit, in tristique ligula laoreet at. Sed id dolor non tortor
        mattis scelerisque. Nunc at porta purus, in mollis eros. Vivamus magna mi,
        hendrerit vitae felis sed, ornare pharetra felis. Fusce volutpat a urna sed mattis.
        Cras magna est, iaculis sed elit in, consequat tristique lorem. Proin egestas fringilla diam non
        vulputate. Maecenas quis tincidunt lacus. Class aptent taciti sociosqu ad litora torquent per
        conubia nostra, per inceptos himenaeos. Donec eu quam venenatis magna commodo imperdiet sit amet
        eget enim.
      </p>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m6 l3">
      <img src="assets/img/img-gr1.jpg" class="img-vignette" alt="image du meilleur burger">
    </div>
    <div class="col s12 m6 l3">
      <img src="assets/img/img-gr2.jpg" class="img-vignette" alt="image du meilleur burger">
    </div>
    <div class="col s12 m6 l3">
      <img src="assets/img/img-gr3.jpg" class="img-vignette" alt="image du meilleur burger">
    </div>
    <div class="col s12 m6 l3">
      <img src="assets/img/img-gr4.jpg" class="img-vignette" alt="image du meilleur burger">
    </div>
  </div>
  <div class="row">
    <div class="col s12">
      <p class="block-text grey-text text-darken-2">
        Cras ultrices luctus aliquam. Cras eros tellus, elementum ut ipsum blandit, mollis euismod elit.
        Cras vitae sem eros. Praesent blandit tempor ligula et facilisis. Nullam eget leo aliquam,
        convallis urna at, consequat nulla. Vestibulum vel metus gravida, imperdiet felis et, mollis urna.
        Integer pretium metus urna, sit amet dapibus dolor lobortis sed. Vestibulum eu semper augue.
        Nulla varius ultricies nulla, a lacinia nisl. Proin vitae ipsum lorem. Donec eget eros eu nisl
        pretium porttitor. Nam posuere sed massa ut scelerisque.
      </p>
    </div>
  </div>

<?php require 'footer.php'; ?>
